#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parson.h"

#include "http.h"


int download_videofile(const char* config_url)
{
    char* page = do_request(config_url);
    if(!page)
    {
        perror("do_request");
        return 1;
    }

    JSON_Value* config_json = json_parse_string(page);
    if(json_value_get_type(config_json) != JSONObject)
    {
        fprintf(stderr, "download_videofile: config is not an object\n");
        json_value_free(config_json);
        free(page);
        return 2;
    }

    JSON_Object* config_object = json_value_get_object(config_json);
    JSON_Array* files = json_object_dotget_array(config_object, "request.files.progressive");
    if(!files)
    {
        fprintf(stderr, "download_videofile: no files found\n");
        json_value_free(config_json);
        free(page);
        return 3;
    }

    const char* URL = NULL;
    size_t URL_len = 0;
    long profile = 0;
    for(size_t i = 0; i < json_array_get_count(files); i++)
    {
        JSON_Object* file = json_array_get_object(files, i);
        const char* file_URL = json_object_get_string(file, "url");
        long file_profile = (int)json_object_get_number(file, "profile");
        if(file_profile > profile)
        {
            profile = file_profile;
            URL = file_URL;
            URL_len = json_object_get_string_len(file, "url");
        }
    }
    if(!URL)
    {
        fprintf(stderr, "download_videofile: no suitable file found\n");
        json_value_free(config_json);
        free(page);
        return 4;
    }

    const char* extension = &URL[URL_len];
    size_t extension_len = 0;
    for(; *extension != '.' && extension_len < URL_len; extension_len++, extension--);

    size_t title_len = json_object_dotget_string_len(config_object, "video.title");
    char* filename = (char*)malloc(title_len + extension_len + 1);
    memcpy(filename, json_object_dotget_string(config_object, "video.title"), title_len);
    memcpy(&filename[title_len], extension, extension_len);
    filename[title_len + extension_len] = 0;

    return do_download(URL, filename);
}
