#ifndef _HTTP_H_
#define _HTTP_H_

char* do_request(const char* URL);
int do_download(const char* URL, const char* filename);


#endif  // _HTTP_H_
